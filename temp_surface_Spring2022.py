import numpy as np
#This set of parameters are for all annealed to 22_22F11_Au-Si-O and the SnOx capped samples
#pfit = [0.0021782225129704067, 0.008989393762476278, -0.07551288955934536, 0.015642531925875964, 3.0791795850693027]
#This set of parameters was used to anneal the samples up to the active learning runs from April 15th
#pfit = [0.0017380204131135426, 0.01182884580650933, -0.07755892460176421, 0.03870934418528416, 3.0347475507057315]


#Si melt pinning (not Au)
#pfit = [ 9.28042059e-03, -4.04355686e-04,  7.25937223e-02, -5.44852557e-01, 3.55016440e+00]

#final for runs
pfit = [0.00903015256, -0.000174216105, 0.0742548071, -0.555849208, 3.5668932]
def temp_surface(b, c, d, e, f):
    # return lambda x, y: (a*x**2+b*x+c)*(y-yth)**(d*x**2+e*x+f) #+ (e*x+f)*(y-yth)
    return lambda x, y: (b*x+c)*(y)**(d*x**2+e*x+f)

def inverse_temp_surface(b,c,d,e,f):
    return lambda dw, t: (t/(b*dw+c))**(1/(d*dw**2 + e*dw +f))

def CO2_svp_conversion(scan_velocity, power):
    """ given a scan velocity and power, return the dwell time and temperature
    scan_velocity is in mm/s
    power is in watts
    """
    logsv = np.log10(scan_velocity)
    fwhm = 0.0882 #mm in scan direction
    t_func = temp_surface(*pfit)
    dwell = fwhm/scan_velocity*10**6 #dwell time in microseconds
    
    temperature = t_func(logsv, power)
    print(f"temperature {temperature} dwell{dwell}")
    return dwell, temperature

def CO2_inversion(dwell, temperature):
    """This takes the dwell and temperature and returns a scan velocity and power"""
    fwhm = 0.0882 #fwhm in scan direction in mm
    logsv = np.log10(fwhm/(dwell/10**6)) #log10 of scan velocity. scan velocity is in mm/s
    sv = fwhm/(dwell/10**6)
    p_func = inverse_temp_surface(*pfit)
    power = p_func(logsv, temperature)
    return sv, power




if __name__ == '__main__':
    #pfit = [0.0022188615494199756, 0.006342578603048725, -0.0789086111648057, 0.011794630904530413, 3.1505144821012436]
    #pfit = [0.002242187478632596, 0.006403974288925822, -0.0789010956543472, 0.011739321594370574, 3.1505871757244295]
    #pfit = [0.0021782225129704067, 0.008989393762476278, -0.07551288955934536, 0.015642531925875964, 3.0791795850693027]
    pfit = [ 9.28042059e-03, -4.04355686e-04,  7.25937223e-02, -5.44852557e-01, 3.55016440e+00]

    t_func = temp_surface(*pfit)
    p_func = inverse_temp_surface(*pfit)
    
    log_velocity = np.log10(9)
    power = 49 
    print(f"At velocity {10**log_velocity}, power {power}W, the predicted temperature is {t_func(log_velocity, power)}")
    print(f"The inverted power is {p_func(log_velocity, t_func(log_velocity, power))}")
