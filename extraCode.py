import numpy as np
import matplotlib.pyplot as plt
import os
import sys



def ZingerBGone(data,mini=0, maxi=9000, fill=1):
    """returns filtered data"""
    data = np.where((mini<=data) & (data<maxi),data,fill)
    return data


def dwellsort(listofkeys,Temp = True):
    demkeys = listofkeys
    temp = []
    s=[]
    for i in demkeys:

        i = i.split('_')
        i[-3] = i[-3].zfill(5)
        i[-1] = i[-1].zfill(5)
        s.append(i)
    s = np.array(s)
#    for i in s[:,1]:
#        i = i.zfill(5)
#        temp.append(i)

#    s=np.insert(s,1,temp,axis=1)
#    temp = []
#    s = np.array(s)
#    s = np.delete(s,2,1)
    if Temp:
        dwellsorted = sorted(s,key=lambda x:(x[-1],x[-3]))
    else:
        dwellsorted = sorted(s,key=lambda x:(x[-3],x[-1]))
#    dwellsorted = sorted(dwellsorted,key=lambda x:x[1])


    for i in dwellsorted:
        i[-3] = i[-3].lstrip('0')
        i[-1] = i[-1].lstrip('0')
        i = np.hstack((i))
        i = i.tolist()
        b = '_'
        i = b.join(i)
        temp.append(i)

    demkeys = temp
    
    return demkeys


def MissingConditions(keys):
    demkeys = dwellsort(keys)
    grid = []
    blanks = []
    for key in demkeys:
        split = key.split('_')
        tau = split[-3]
        T = split[-1]
        grid.append([tau,T])
    conds = np.array(grid,dtype=float)
    uniqueTs = np.unique(conds[:,1])
    uniqueDs = np.unique(conds[:,0])
    for T in uniqueTs:
        empty_dwells = np.array(list(set(conds[conds[:,1]==T][:,0]).symmetric_difference(set(uniqueDs))))
        blank_keys=['tau_'+str(int(i))+'_T_'+str(int(T)) for i in empty_dwells]
        for key in blank_keys:
            blanks.append(key)
    return blanks


def GenerateXmap(tau=10000,Tmax=1400,h5file=None):
    data = h5.File(h5file, 'r')['exp']
    key = f'tau_{tau}_T_{Tmax}'
    xmap = []
    for subscan in sorted(list(data[key]), key=int):
        xmap.append(data[key][subscan]['integrated_1d'][1])
        xmap = np.array(xmap)
        Q = data[key][subscan]['integrated_1d'][0]
    return xmap, Q

def QtoTT(Q,wl=0.15418):
    """ Returns the two theta value given a scattering vector, Q, and an x-ray wavelength in nm"""
    return 2*np.rad2deg(np.arcsin(Q/(4*np.pi)*wl))

def TTtoQ(TT,wl=0.15418):
    """ Returns the scattering vector magnitued given a two theta value, TT, and an x-ray wavelength in nm"""
    return 4*np.pi*np.sin(np.deg2rad(TT/2))/wl


def KeyWhere(Dict, subkeys=[], values=[], attr=False):
    """
    For the prototypical dictionary structure, we want to know the head key value where some set of conditions exist
    
    returns all the keys where the values == subkeys in order
    """
    allkeys = list(Dict)
    headkeys = []
    #print(values,len(values))
    for key in allkeys:
#         print([float(Dict[key][subkeys[i]]) == values[i] for i in range(len(values))])
        if attr:
            if np.all([float(Dict[key].attrs[subkeys[i]]) == values[i] for i in range(len(values))]):
                headkeys.append(key)
        else:
            if np.all([float(Dict[key][subkeys[i]]) == values[i] for i in range(len(values))]):
                headkeys.append(key)
        
    return headkeys

def JitterCorrection(xmap):
    """This will remove some of the jitter in an XRD map due to the detector binning incorrectly
    The mean of each column (1D XRD scan) is collected into an array. the mean of the means is
    determined and a uniform shift is applied based on the differnece between the mean of a column
    and the overall mean so that they are all the same.
    
    Basically, the average intensity of each scan should be the same
    """
    temp = []
    newmap=[]
    for idx in np.arange(xmap.shape[1]):
        col = xmap[:,idx]
        temp.append(np.mean(col))
    meanofmeans = np.mean(temp)
    diffs = [mean-meanofmeans for mean in temp]
    for idx in np.arange(xmap.shape[1]):
        col = xmap[:,idx]-diffs[idx]
        newmap.append(col)
        
    return np.array(newmap).T
